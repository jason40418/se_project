from functools import wraps
from flask import Flask, make_response, render_template, request, redirect, jsonify, url_for
from lib.mod.dbmgr import DBMgr
from lib.mod.member import Member
from os.path import join, dirname, realpath
import jwt
import time
import re
from pprint import pprint
import datetime

work_dict = dirname(realpath(__file__))

public_key_file = open(join(dirname(realpath(__file__)), 'key/public.pem'), 'r', encoding='UTF-8')
public_key = public_key_file.read()
private_key_file = open(join(dirname(realpath(__file__)), 'key/private.pem'), 'r', encoding='UTF-8')
private_key = private_key_file.read()

app = Flask(__name__)

dbMgr = DBMgr(work_dict)


# 從 token 拿到 user
def get_account_from_token():
    token = request.cookies.get('token')
    payload = jwt.decode(token, public_key, algorithms=['RS512'])
    # print(payload)
    return payload['account']

def get_user_from_token():
    token = request.cookies.get('token')
    if token is None:
        return False
    try:
        payload = jwt.decode(token, public_key, algorithms=['RS512'])
        member = dbMgr.getMember(payload['account'])
        return member.getMember()
    # decode error or expired
    except (jwt.DecodeError, jwt.ExpiredSignatureError):
        return False


def is_keys_exist(data, keys=[]):
    not_found_keys = []
    for key in keys:
        if key not in data:
            not_found_keys.append(key)
    if len(not_found_keys) != 0:
        return False, not_found_keys
    else:
        return True, None

def is_login():
    token = request.cookies.get('token')
    if token is None:
        return False
    try:
        payload = jwt.decode(token, public_key, algorithms=['RS512'])
        print(payload)
        return True
    # decode error or expired
    except (jwt.DecodeError, jwt.ExpiredSignatureError):
        return False


# 檢查有沒有 token 且是否能 decode 的 decorator @login_required_page
def login_required_page(f):
    # Required user to login
    @wraps(f)
    def wrapper(*args, **kwds):
        token = request.cookies.get('token')
        if token is None:
            return direct_to_login()
        try:
            payload = jwt.decode(token, public_key, algorithms=['RS512'])
            print(payload)
            return f(*args, **kwds)
        # decode error or expired
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return direct_to_login()

    def direct_to_login():
        resp = make_response(redirect(url_for('get_login_page')), 302)
        resp.set_cookie('token', '', expires=0)
        return resp
    return wrapper

def login_required_page_admin(f):
    # Required user to login
    @wraps(f)
    def wrapper(*args, **kwds):
        token = request.cookies.get('token')
        if token is None:
            return direct_to_login()
        try:
            # payload = jwt.decode(token, public_key, algorithms=['RS512'])
            # print(payload)
            account = get_account_from_token()
            manager = dbMgr.getMember(account)
            if manager.getIdentity() != 'admin':
                return direct_to_login()
            return f(*args, **kwds)
        # decode error or expired
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return direct_to_login()

    def direct_to_login():
        resp = make_response(redirect(url_for('admin_login')), 302)
        resp.set_cookie('token', '', expires=0)
        return resp
    return wrapper

def login_required_api(f):
    # Required user to login
    @wraps(f)
    def wrapper(*args, **kwds):
        token = request.cookies.get('token')
        if token is None:
            return jsonify({
                "error_msg": "尚未登入",
                "error_type": "LoginRequiredError"
            }), 403
        try:
            payload = jwt.decode(token, public_key, algorithms=['RS512'])
            print(payload)
            return f(*args, **kwds)
        # decode error or expired
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return jsonify({
                "error_msg": "token 過期惹",
                "error_type": "TokeExpireError"
            }), 403

    return wrapper


#########
# route #
#########

######
# 01 #
######
@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
def index():
    resp = make_response(render_template('app/index.html', is_login = is_login(), user_profile = get_user_from_token()))
    return resp


######
# 02 #
######
@app.route('/login', methods=['GET', 'POST', 'PATCH', 'DELETE'])
def get_login_page():
    if is_login():
        resp = make_response(redirect('/member'))
    else:
        resp = make_response(render_template('app/login.html'))
    return resp


######
# 03 #
######
# 登入
# return cookie with token
@app.route('/api/login', methods=['POST'])
def api_login():
    data = request.json
    is_exist, keys = is_keys_exist(data, ['account', 'password'])

    if not is_exist:
        return jsonify({
            "error_msg": "有些 key 沒有傳入",
            "error_type": "KeyNotFoundError",
            "keys": keys
        }), 400

    account = request.json['account']
    password = request.json['password']

    is_auth, error_msg, error_type = dbMgr.authorizeMember(account, password)
    if is_auth:
        member = dbMgr.getMember(account)
        resp = make_response(jsonify({"msg": "登入成功"}), 200)
        expired_time = time.time() + 6 * 60
        # 使用 jwt 加密
        payload = {
            "account": account,
            "name": member.getName(),
            "expired_time": expired_time
        }
        token = jwt.encode(payload, private_key, algorithm='RS512')

        # 設定 cookies
        resp.set_cookie(key='token', value=token, expires=expired_time)

        return resp
    else:
        return jsonify({
            "error_msg": error_msg,
            "error_type": error_type,
        }), 400


######
# 04 #
######
@app.route('/register', methods=['GET'])
def get_register_page():
    if is_login():
        resp = make_response(redirect('/member'))
    else:
        resp = make_response(render_template('app/register.html'))
    return resp


######
# 05 #
######
# 註冊功能
@app.route('/api/register', methods=['POST'])
def api_register():
    data = request.json
    is_exist, keys = is_keys_exist(data, ['account', 'password', 'name'])

    if not is_exist:
        return jsonify({
            "error_msg": "有些 key 沒有傳入",
            "error_type": "KeyNotFoundError",
            "keys": keys
         }), 400

    member_data = data
    member_data['identity'] = 'member'
    member_data['createDateTime'] = datetime.datetime.now()
    # 檢查格式
    reg_str = r'^[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+){0,4}@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+){1,4}$'
    if re.match(reg_str, member_data['account']):
        # 帳號是否重複
        is_member_duplicate = dbMgr.isMemberDuplicate(member_data['account'])
        if is_member_duplicate:
            # 如果重複
            return jsonify({
                "error_msg": "帳號已重複",
                "error_type": "AccountDuplicatedError"
            }), 400

        m = Member(member_data)
        if not dbMgr.addMember(m):
            return jsonify({
                "error_msg": "發生錯誤！",
                "error_type": "DBError"
            }), 400

        return jsonify({"msg": "註冊成功"}), 200

    else:
        return jsonify({
            "error_msg": "帳號格式錯誤",
            "error_type": "AcconutFormatError"
        }), 400


######
# 06 #
######
# 拿到個人資料與頁面
@app.route('/member', methods=['GET'])
@login_required_page
def get_member():
    account = get_account_from_token()
    m = dbMgr.getMember(account)

    resp = make_response(render_template('app/profile.html', is_login = is_login(), user_profile = m.getMember()))
    return resp
    # return jsonify({
    #     "msg": "檢視會員資料成功",
    #     "member": m.getMember()
    # }), 200


######
# 07 #
######
# 更新個人資料
@app.route('/api/member', methods=['PATCH'])
@login_required_api
def patch_member():
    data = request.json
    is_exist, keys = is_keys_exist(data, ['name', 'oldPassword', 'newPassword'])

    if not is_exist:
        return jsonify({
            "error_msg": "有些 key 沒有傳入",
            "error_type": "KeyNotFoundError",
            "keys": keys
         }), 400

    account = get_account_from_token()
    m = dbMgr.getMember(account)

    edit_member_data = data
    result = m.editMember(edit_member_data['name'], edit_member_data['oldPassword'], edit_member_data['newPassword'])
    if result == "passwordError":
        return jsonify({
                "error_msg": "輸入密碼和原密碼不相符",
                "error_type": "MemberPasswordError"
        }), 400

    else:
        result = dbMgr.updateMember(m)
        if result:
            return jsonify({
                "msg": "更新會員資料成功",
                "member": m.getMember()
            }), 200
        else:
            return jsonify({
                "error_msg": "更新失敗",
                "error_type": "UpdateMemberError"
            }), 400


######
# 08 #
######
# 刪除帳號
@app.route('/api/member/<string:target>', methods=['DELETE'])
@login_required_api
def delete_member(target):
    # 原來身份
    account = get_account_from_token()
    print(account)
    # 判斷時不是管理員
    member = dbMgr.getMemberIdentity(account)

    # 刪除自己
    if target == 'me':
        # 管理員不能刪除自己
        if member['identity'] == "admin":
            return jsonify({
                "error_msg": "不能刪除管理員",
                "error_type": "CanNotDeleteAdminError"
            }), 400

        target = account

    # 管理員刪除別人
    else:
        # 先看看有沒有這個會員
        targetMember = dbMgr.getMember(target)
        if not targetMember:
            return jsonify({
                "error_msg": "無此會員",
                "error_type": "CanNotFoundMemberError"
            }), 400

        # 判斷是不是管理員
        if member['identity'] == "admin":
            # 管理員不能刪除管理員
            if targetMember.getIdentity() == "admin":
                return jsonify({
                    "error_msg": "不能刪除管理員",
                    "error_type": "CanNotDeleteAdminError"
                }), 400
            target = targetMember.getAccount()
        else:
            if targetMember.getAccount() != account:
                return jsonify({
                    "error_msg": "管理員權限",
                    "error_type": "NotAdminError"
                }), 403

    #先刪除該會員所有收藏資料

    deleteAllFavoriteResult = dbMgr.deleteMemberAllFavorite(target)
    if deleteAllFavoriteResult:
        result = dbMgr.deleteMember(target)

        if result:
            return jsonify({
                "msg": "刪除會員成功"
             }), 200
        else:
            return jsonify({
                "error_msg": "刪除會員失敗",
                "error_type": "AccountNotFoundError",
            }), 400

    else:
        return jsonify({
            "error_msg": "刪除收藏失敗,資料庫錯誤",
            "error_type": "DeleteFavoriteError",
        }), 400




######
# 09 #
######
# 取得展場列表
@app.route('/exhibitions', methods=['GET'])
def get_exhibitions():
    # 檢查 url 參數
    title = request.args.get('title')
    categories = request.args.get('categories')
    if (categories):
        categories = categories.split(',')
    where = request.args.get('where')
    start = request.args.get('start')
    end = request.args.get('end')
    # [title, category, where, start, end]
    filters = {
        "title": title,
        "categories": categories,
        "where": where,
        "time":
        {
            "start": (start + " 00:00:00" if start else "1990-01-01 00:00:00"),
            "end": (end +" 23:59:59" if end else "9999-12-31 23:59:59")
        }
    }
    # print(filters)
    result, exhibitionList = dbMgr.getExhibitionList(filters)
    countResult, count_list = dbMgr.countFavoriteTimes()
    count = {}
    if countResult:
        for each in count_list:
            count[each['UID']] = each['times']

    # 判斷有無登入，以取得收藏列表
    favorite_list = []
    if is_login():
        user = True
        account = get_account_from_token()
        favorites = dbMgr.getFavorite(account)
        if favorites != False:
            for each in favorites:
                uid = each['UID']
                favorite_list.append(uid)
    else:
        user = False

    # 判斷是否有搜尋結果
    state = True if result else False

    resp = make_response(render_template('app/show.html', state = state, exhibition = exhibitionList, is_login = user, user_profile = get_user_from_token(), favorite = favorite_list, favorite_times = count, favorite_times_result = countResult))
    return resp


######
# 10 #
######
# 取得某展場的詳細資訊（包含場次）
@app.route('/exhibitions/<string:uid>', methods=['GET'])
def get_exhibitions_by_id(uid):
    # TODO 檢查有無登入，若有登入，要標記是否有收藏展場（追蹤場次）
    is_found, exhibition = dbMgr.getExhibition(uid)

    result_data = {}
    if is_found:
        categoryId =  exhibition.getData()['category']
        categoryName = dbMgr.getCategoryName(categoryId)['categoryName']
        sessions = dbMgr.getSessionList(exhibition.getUid())
        result_data = {
            'exhibition': exhibition.getData(),
            'sessions': {}
        }
        # 取得類別名稱
        result_data['exhibition']['categoryName'] = categoryName

        result_data['exhibition']['discountInfo'] = result_data['exhibition']['discountInfo'].replace('<br />', '\\r\\n')

        print(result_data['exhibition']['discountInfo'])
        print(exhibition.getData())
        for s in sessions:
            s_data = s.getData()
            # 取得場次地點ID
            locationID = dbMgr.getSessionLocation(s_data['uid'], s_data['seq'])['locationId']
            locationData = dbMgr.getLocationData(locationID)
            result_data['sessions'][s_data['seq']] = s_data
            result_data['sessions'][s_data['seq']]['location'] = locationData

        # 取得總收藏次數
        countResult, count = dbMgr.countSingleFavoriteTimes(uid)
        result_data['exhibition']['count'] = count if countResult else 0

    # 判斷有無登入，以取得使用者收藏列表
    favorite_list = []
    if is_login():
        user = True
        account = get_account_from_token()
        favorites = dbMgr.getFavorite(account)
        if favorites != False:
            for each in favorites:
                uid = each['UID']
                favorite_list.append(uid)
    else:
        user = False

    resp = make_response(render_template('app/showdetail.html', is_found = is_found, result = result_data, is_login = user, user_profile = get_user_from_token(), favorite = favorite_list))
    return resp


######
# 11 #
######
# 取得收藏列表
@app.route('/favorites', methods=['GET'])
@login_required_page
def get_favorites():
    account = get_account_from_token()
    result = dbMgr.getFavorite(account)
    if result != False:
        favorite_list = []
        for each in result:
            uid = each['UID']
            is_found, exhibition = dbMgr.getExhibition(uid)
            title = exhibition.getData()['title']
            categoryId =  exhibition.getData()['category']
            categoryName = dbMgr.getCategoryName(categoryId)['categoryName']
            favorite = {
                'UID': uid,
                'title': title,
                'categoryId': categoryId,
                'categoryName': categoryName
            }
            favorite_list.append(favorite)
        print(favorite_list)
        resp = make_response(render_template('app/like.html', favorite_list = favorite_list, is_login = is_login(), user_profile = get_user_from_token()))
        return resp

    else:
        return jsonify({
            "error_msg": "取得收藏列表失敗",
            "error_type": "GetFavoriteError",
        }), 400


######
# 12 #
######
# 新增收藏
@app.route('/api/favorites', methods=['POST'])
@login_required_api
def post_favorites():
    account = get_account_from_token()
    data = request.json
    is_exist, keys = is_keys_exist(data, ['UID'])

    if not is_exist:
        return jsonify({
            "error_msg": "有些 key 沒有傳入",
            "error_type": "KeyNotFoundError",
            "keys": keys
        }), 400

    favorite_data = data
    is_found, exhibition = dbMgr.getExhibition(favorite_data['UID'])
    if is_found:
        result = dbMgr.addFavorite(favorite_data['UID'], account)

        if result:
            return jsonify({
                    "msg": "新增收藏成功",
            }), 200
        else:
            return jsonify({
                "error_msg": "新增收藏失敗,不可重複收藏",
                "error_type": "AddFavoriteError",
            }), 400
    else:
        return jsonify({
                "error_msg": "展場id不存在",
                "error_type": "UIDError",
        }), 400


######
# 14 #
######
# 刪除收藏
@app.route('/api/favorites', methods=['DELETE'])
@login_required_api
def delete_favorites():
    account = get_account_from_token()
    data = request.json
    is_exist, keys = is_keys_exist(data, ['UID'])

    if not is_exist:
        return jsonify({
            "error_msg": "有些 key 沒有傳入",
            "error_type": "KeyNotFoundError",
            "keys": keys
        }), 400

    favorite_data = data
    result = dbMgr.deleteFavorite(favorite_data['UID'], account)
    if result:
        return jsonify({
            "msg": "刪除收藏成功",
        }), 200
    else:
        return jsonify({
            "error_msg": "刪除收藏失敗,不存在此收藏",
            "error_type": "DeleteFavoriteError",
        }), 400


# 取得類別列表
@app.route('/categoryList', methods=['GET'])
def get_category_list():
    result = dbMgr.getAllCategoryAndName()
    if result:
        return jsonify({
            "msg:": "檢視類型列表成功",
            "categoryList:": result
        }), 200

    else:
        return jsonify({
            "error_msg": "取得類型列表失敗",
            "error_type": "GetCategoryListError",
        }), 400


# Deprecated
# testing
@app.route('/authorization', methods=['POST'])
def authorization():
    # 接收 post 資料
    result = request.form
    resp = make_response(redirect('/index'))

    # 使用 jwt 加密
    payload = {
        "account": result['account'],
        "expire_time": time.time() + 6 * 60
    }
    token = jwt.encode(payload, private_key, algorithm='RS512')

    # 設定 cookies
    resp.set_cookie(key='token', value=token, expires=time.time() + 6 * 60)

    return resp
# ==========================================================================================================================

###############
# 管理員管理功能 #
###############

######
# 管理員登入
# return cookie with token
@app.route('/api/admin_login', methods=['POST'])
def api_admin_login():
    data = request.json
    is_exist, keys = is_keys_exist(data, ['account', 'password'])

    if not is_exist:
        return jsonify({
            "error_msg": "有些 key 沒有傳入",
            "error_type": "KeyNotFoundError",
            "keys": keys
        }), 400

    account = request.json['account']
    password = request.json['password']

    is_auth, error_msg, error_type = dbMgr.authorizeMember(account, password)
    if is_auth:
        manager = dbMgr.getMember(account)
        if manager.getIdentity() != 'admin':
            return jsonify({
                'error_msg': 'You are not admin.',
                'error_type': 'IdentityError'
            }), 403
        resp = make_response(jsonify({"msg": "登入成功"}), 200)
        expired_time = time.time() + 6 * 60
        # 使用 jwt 加密
        payload = {
            "account": account,
            "expired_time": expired_time
        }
        token = jwt.encode(payload, private_key, algorithm='RS512')

        # 設定 cookies
        resp.set_cookie(key='token', value=token, expires=expired_time)

        return resp
    else:
        return jsonify({
            "error_msg": error_msg,
            "error_type": error_type,
        }), 400

@app.route('/admin/login', methods=['GET'])
def admin_login():
    resp = make_response(render_template('admin/login.html'))
    return resp


@app.route('/admin/', methods=['GET'])
@app.route('/admin/index', methods=['GET'])
@login_required_page_admin
def admin_index():
    dash = dbMgr.getDashboard()
    if dash != False:
        dashboard_list = {}
        for each in dash:
            dashboard_list[each['table_name']] = each['table_rows']

    else:
        return jsonify({
            "error_msg": "取得儀錶板失敗",
            "error_type": "GetDashboardError",
        }), 400
    print(dashboard_list)
    resp = make_response(render_template('admin/index.html', dashboard = dashboard_list))
    return resp

# admin 拿會員列表
@app.route('/admin/members', methods=['GET'])
@login_required_page_admin
def get_members():
    account = get_account_from_token()
    manager = dbMgr.getMember(account)
    if manager.getIdentity() != 'admin':
        return jsonify({
            'error_msg': 'You are not admin.',
            'error_type': 'IdentityError'
        }), 403
    member_list = dbMgr.getAllMember()
    if member_list:
        result = {
            "state": True,
            "memberList": member_list
        }

    else:
        result = {
            "state": False,
            "error_type": "GetCategoryListError",
        }
    resp = make_response(render_template('admin/members.html', result = result))
    return resp

# admin 檢視單個會員
@app.route('/admin/member/<string:target>', methods=['GET'])
@login_required_page_admin
def get_single_member(target):
    account = get_account_from_token()
    manager = dbMgr.getMember(account)
    if manager.getIdentity() != 'admin':
        return jsonify({
            'error_msg': 'You are not admin.',
            'error_type': 'IdentityError'
        }), 403

    # TODO 若使用網址輸入管理員帳號，應該予以排除
    # 取得單個會員資料
    m = dbMgr.getMember(target)
    if not m:
        return jsonify({
            "error_msg": "取得會員資訊失敗",
            "error_type": "GetMemberError",
        }), 400

    result = dbMgr.getFavorite(target)
    if result != False:
        favorite_list = []
        for each in result:
            uid = each['UID']
            is_found, exhibition = dbMgr.getExhibition(uid)
            title = exhibition.getData()['title']
            categoryId =  exhibition.getData()['category']
            categoryName = dbMgr.getCategoryName(categoryId)['categoryName']
            favorite = {
                'UID': uid,
                'title': title,
                'categoryId': categoryId,
                'categoryName': categoryName
            }
            favorite_list.append(favorite)

    else:
        return jsonify({
            "error_msg": "取得收藏列表失敗",
            "error_type": "GetFavoriteError",
        }), 400

    resp = make_response(render_template('admin/member.html', member = m.getMember(), favorite = favorite_list, result = result))
    return resp


# admin 拿類別列表頁面
@app.route('/admin/categories', methods=['GET'])
@login_required_page_admin
def get_categories_pages():
    account = get_account_from_token()
    manager = dbMgr.getMember(account)
    if manager.getIdentity() != 'admin':
        return jsonify({
            'error_msg': 'You are not admin.',
            'error_type': 'IdentityError'
        }), 403
    categories_list = dbMgr.getAllCategoryAndName()
    if categories_list:
        result = {
            "state": True,
            "categoryList": categories_list
        }

    else:
        result = {
            "state": False,
            "error_type": "GetCategoryListError",
        }
    resp = make_response(render_template('admin/categories.html', result = result))
    return resp

# 修改類別名稱
@app.route('/api/categoryList', methods=['PATCH'])
@login_required_api
def post_category_list():
    account = get_account_from_token()
    manager = dbMgr.getMember(account)
    if manager.getIdentity() != 'admin':
        return jsonify({
            'error_msg': 'You are not admin.',
            'error_type': 'IdentityError'
        }), 403

    data = request.json
    is_exist, keys = is_keys_exist(data, ['id', 'value'])

    print(data)

    if not is_exist:
        return jsonify({
            "error_msg": "有些 key 沒有傳入",
            "error_type": "KeyNotFoundError",
            "keys": keys
        }), 400

    categoryData = data
    updateResult = dbMgr.updateCategoryData(categoryData['id'], categoryData['value'])
    if updateResult == True:
        return jsonify({
            "msg": "修改類別資料成功",
        }), 200
    else:
        return jsonify({
            "error_msg": "修改類別資料失敗，無此類別Id",
            "error_type": "UpdateCategoryError",
    }), 400


##########
# logout #
##########
@app.route('/logout')
def logout():
    resp = make_response(redirect(url_for('index')), 302)
    resp.set_cookie('token', '', expires=0)
    return resp


###############
# Error Pages #
###############


# if 404 not found, redirect to index
@app.errorhandler(404)
def page_not_found(e):
    # redirect to index
    resp = make_response(redirect('/index'))
    return resp


# 處理靜態（static）文件路徑
@app.route('/<path:path>')
def static_file(path):
    return app.send_static_file(path)


# debug=True 可讓更新後即時顯示，不用重開
if __name__ == '__main__':
    app.run(debug=True, port=5000, threaded=True, processes=4)
    app.run()
