import urllib.request, json
import mod

def setUnitList(data):
    unit_list = []

    for each_master in data:
        curr_unit = mod.Unit()
        curr_unit.setName(each_master)

        # 取得單位編號，並設定
        get_unit_id = dbmgr.getUnitId(curr_unit)
        curr_unit.setID(get_unit_id)
        unit_list.append(curr_unit)

        del curr_unit

    return unit_list

def setOrganizerList(uid, data, roleType):
    organizer_list = []

    for each_unit in data:
        curr_organizer = mod.Organizer()
        curr_organizer.setUID(uid)
        curr_organizer.setUnit(each_unit)
        curr_organizer.setType(roleType)
        organizer_list.append(curr_organizer)
        del curr_organizer

    return organizer_list

# 使用 資料庫管理檔案
dbmgr = mod.DBMgr()

# 1.取得網站「json」
# ===================================================
# 設定網址
web_link = "https://cloud.culture.tw/frontsite/trans/SearchShowAction.do?method=doFindTypeJ&category=all"

# 開啟網址並將取得網站內容解析成 json 格式
with urllib.request.urlopen(web_link) as url:
    data = json.loads(url.read().decode())

# 取得目前資料庫有哪些「展覽類別」
category_query = dbmgr.getAllCategory()
category_list = []
for each in category_query:
    category_list.append(each["categoryId"])



# 2.處理每筆資料
# ===================================================
count = 0
finish_rate = 0.0
for deal_data in data:
    count += 1
    finish_rate = count / len(data)

    # STEP 1：設定「展覽類別」
    curr_category = mod.Category()
    curr_category.setID(deal_data["category"])

    if(curr_category.getID() in category_list):
        pass
    else:
        dbmgr.addCategory(curr_category)
        category_list.append(curr_category.getID())

    del curr_category

    # STEP 2：設定「展覽資料」
    curr_exhibition = mod.Exhibition()
    curr_exhibition.setData(deal_data)

    print("開始處理：%s" %(curr_exhibition.getUid()))

    is_duplicated = dbmgr.isExhibitionDuplicate(curr_exhibition.getUid())

    if(is_duplicated):
        dbmgr.updateExhibition(curr_exhibition)
        print("更新展覽資訊完成")
    else:
        dbmgr.addExhibition(curr_exhibition)
        print("新增展覽資訊完成")

    # STEP 3：設定「主辦單位」、「協辦單位」、「贊助單位」、「其他單位」
    curr_master_unit = setUnitList(deal_data["masterUnit"])
    curr_sub_unit = setUnitList(deal_data["subUnit"])
    curr_support_unit = setUnitList(deal_data["supportUnit"])
    curr_other_unit = setUnitList(deal_data["otherUnit"])

    curr_organizer_list = []
    curr_organizer_list.extend(setOrganizerList(curr_exhibition.getUid(), curr_master_unit, "master"))
    curr_organizer_list.extend(setOrganizerList(curr_exhibition.getUid(), curr_sub_unit, "sub"))
    curr_organizer_list.extend(setOrganizerList(curr_exhibition.getUid(), curr_support_unit, "support"))
    curr_organizer_list.extend(setOrganizerList(curr_exhibition.getUid(), curr_other_unit, "other"))

    dbmgr.deleteSingleExhibitionUnit(curr_exhibition.getUid())

    for each_organizer in curr_organizer_list:
        dbmgr.addOrganizer(each_organizer)
        print("正在處理單位關係：%s" %(each_organizer.getUnit().getName()))

    # 刪除產生的物件
    del curr_master_unit, curr_sub_unit, curr_support_unit, curr_other_unit, curr_organizer_list

    # STEP 4：建立「場次」和「地點」物件
    if(len(data) != 0):
        if(len(deal_data["showInfo"]) == 1 and deal_data["showInfo"][0] == {}):
            pass
        else:
            sequence = 0
            # 將「地點」與「場次」關係清除
            dbmgr.deleteSessionLocationList(curr_exhibition.getUid())
            # 將「場次」清除
            dbmgr.deleteSessionList(curr_exhibition.getUid())

            for each in deal_data["showInfo"]:
                sequence += 1
                curr_session = mod.Session()
                curr_location = mod.Location()
                curr_session_location = mod.SessionLocation()

                # 將場次存入資料庫當中
                curr_session.setData(curr_exhibition.getUid(), sequence, each["time"], each["onSales"], each["price"], each["endTime"])
                dbmgr.addSession(curr_session)

                # 將地點存入資料庫或取出資料庫當中
                curr_location.setData(0, each["location"], each["locationName"], each["latitude"], each["longitude"])
                curr_location_ID = dbmgr.getLocationID(curr_location)
                curr_location.setID(curr_location_ID)

                curr_session_location.setSession(curr_session)
                curr_session_location.setLocation(curr_location)
                dbmgr.addSessionLocation(curr_session_location)

                print("正在處理地點場次關係：%s, %s" %(sequence, each["locationName"]))

                del curr_session
                del curr_location

            del sequence
    else:
        pass

    print("完成：%s" %(curr_exhibition.getUid()))
    print("已處理：%d/%d、完成率：%.2f%%" %(count, len(data), (finish_rate * 100)))
    print("-" * 20)
    del curr_exhibition



# 3.更新展覽類別 json 對照
# ===================================================
with open('json/category.json', encoding = 'utf8') as category:
    data = json.load(category)

for each in data["data"]:
    category = mod.Category()
    category.setID(each["id"])
    category.setName(each["tc_name"])

    if(category.getID() in category_list):
        # 更新展覽類別
        dbmgr.updateCategory(category)
    else:
        # 新增展覽類別
        dbmgr.addCategory(category)
