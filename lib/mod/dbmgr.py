import pymysql, bcrypt

from .config import Config
from .member import Member
from .exhibition import Exhibition
from .session import Session

class DBMgr:
    # 1. 連接 PyMySQL 的基本功能
    # ==================================================================================
    def __init__(self, route=''):
        self.state = False
        self.cfg = Config(route)

    def mysql_error(self):
        return pymysql.MySQLError

    def conn(self):
        self.database_info = self.cfg.getDatabase()
        try:
            self.connection = pymysql.connections.Connection(
                host = self.database_info["host"],
                port = self.database_info["port"],
                user = self.database_info["user"],
                password = self.database_info["passwd"],
                db = self.database_info["db"],
                charset = self.database_info["charset"],
                cursorclass=pymysql.cursors.DictCursor
            )
            self.state = True
            return self.connection

        except self.mysql_error() as e:
            self.state = False
            print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
            self.conn()

    def cursor(self):
        try:
            self.connection
        except:
            self.conn()

        return self.connection.cursor()

    def commit(self):
        try:
            self.connection
        except:
            self.conn()

        return self.connection.commit()

    def close(self):
        try:
            self.connection
        except:
            self.conn()
        else:
            self.cursor().close()
            self.connection.close()
            del self.connection

    # 2. category.py 模組
    # ==================================================================================
    def addCategory(self, category):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "INSERT INTO   `category` VALUES(%(ID)s, %(name)s)"
                    args = {
                        "ID": str(category.getID()),
                        "name": category.getName()
                    }

                    cursor.execute(sql, args)
                    self.commit()

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    def updateCategory(self, category):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "UPDATE   `category` SET  `categoryName` = %(name)s   WHERE `categoryId` = %(ID)s"
                    args = {
                        "ID": str(category.getID()),
                        "name": category.getName()
                    }

                    cursor.execute(sql, args)
                    self.commit()

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    def getAllCategory(self):
        num_of_rows = 0
        data = []

        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT   `categoryId`   FROM `category`"

                    num_of_rows = int(cursor.execute(sql))

                    data = cursor.fetchall() if(num_of_rows != 0) else []

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))



        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

        return data
    #取得類型列表
    def getAllCategoryAndName(self):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT   *   FROM `category`"

                    num_of_query = int(cursor.execute(sql))
                    if(num_of_query > 0):
                        categoryList = cursor.fetchall()
                        return categoryList

                    else:
                        return False

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    #查詢類別中文名稱
    def getCategoryName(self,data):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT   `categoryName`   FROM `category` WHERE   `categoryId` = %(value)s"
                    args = {"value": data}

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query == 1):
                        categoryName = cursor.fetchone()
                        return categoryName

                    else:
                        return False

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    #修改類別名稱
    def updateCategoryData(self, categoryId, categoryName):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "UPDATE `category` SET `categoryName` = %(name)s where `categoryId` =  %(Id)s"
                    args = {
                        "Id": categoryId,
                        "name": categoryName
                    }

                    cursor.execute(sql, args)
                    self.commit()
                    self.close()

                    return True

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                self.close()
                return False

        else:
            print("Fails to connect to MySQL Server!!")
            return False

    # 3. exhibition.py 模組
    # ==================================================================================
    def addExhibition(self, exhibition):
        if( self.conn() ):
            try:
                sql = "INSERT INTO  `exhibition`    VALUES(%(UID)s, %(title)s, %(category)s, %(discountInfo)s, %(descriptionFilterHtml)s, %(imageUrl)s, %(webSales)s)"
                args = exhibition.getData()

                self.cursor().execute(sql, args)
                self.commit()

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    def isExhibitionDuplicate(self, data):
        num_of_query = -1

        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT   *   FROM `exhibition`   WHERE   `uid` = %(value)s"
                    args = {"value": data}

                    num_of_query = int(cursor.execute(sql, args))

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

        return True if(num_of_query == 1) else False

    def updateExhibition(self, exhibition):
        if( self.conn() ):
            try:
                sql = "UPDATE  `exhibition`    SET  `title` = %(title)s, `categoryId` = %(category)s, `discountInfo` = %(discountInfo)s, `descriptionFilterHTML` = %(descriptionFilterHtml)s, `imgUrl` = %(imageUrl)s, `webSales` = %(webSales)s    WHERE `uid` = %(UID)s"
                args = exhibition.getData()

                self.cursor().execute(sql, args)
                self.commit()

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()
    #根據uid檢視展場資訊
    def getExhibition(self, uid):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT * FROM `exhibition` WHERE `uid` = %(value)s;"
                    args = {"value": uid}

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query == 1):
                        exhibitionData = cursor.fetchone()
                        print("檢視展場成功")
                        exhibitionData['UID'] = exhibitionData['uid']
                        exhibitionData['category'] = exhibitionData['categoryId']
                        exhibitionData['descriptionFilterHtml'] = exhibitionData['descriptionFilterHTML']
                        exhibitionData['imageUrl'] = exhibitionData['imgUrl']
                        exhibitionData['webSales'] = exhibitionData['websales']
                        e = Exhibition()
                        e.setData(exhibitionData)
                        return True, e

                    else:
                        print("檢視展場失敗")
                        return False, None

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False, None
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()
        return False, None

    #根據uid檢視展場資訊
    def getExhibitionList(self, filters):

        sql = 'SELECT DISTINCT `exhibition`.`uid`, `exhibition`.`title`, `exhibition`.`categoryId`, `category`.`categoryName` FROM `exhibition`, `category` WHERE `exhibition`.`categoryId` = `category`.`categoryId` AND `exhibition`.`uid` in (SELECT DISTINCT `exhibition`.`uid` FROM `exhibition`, `session`, `sessionlocation` , `location`, `category` WHERE `exhibition`.`uid` = `session`.`UID` AND `exhibition`.`uid` = `sessionlocation`.`UID` AND `session`.`UID` = `sessionlocation`.`UID` AND `sessionlocation`.`locationId` = `location`.`locationId` AND `exhibition`.`categoryId` = `category`.`categoryId`'

        if filters['title']:
            sql += ' AND REPLACE(`exhibition`.`title`, \" \", \"\") LIKE REPLACE("%' + filters['title'] + '%", \" \", \"\")'
        if filters['categories']:
            sql += ' AND ('
            categoryNum = 1
            for category in filters['categories']:
                sql += ' `exhibition`.`categoryId` = ' + category
                if (categoryNum != len(filters['categories'])):
                    sql += ' OR'
                categoryNum += 1
            sql += ')'
        if filters['where']:
            sql += ' AND (REPLACE(`location`.`location`, \" \", \"\") LIKE REPLACE("%' + filters['where'] + '%", \" \", \"\")'
            sql += ' OR REPLACE(`location`.`locationName`, \" \", \"\") LIKE REPLACE("%' + filters['where'] + '%", \" \", \"\"))'

        if filters['time']['start'] != '1990-01-01 00:00:00':
            sql += ' AND `session`.`time` >= "' + filters['time']['start'] + '"'

        if filters['time']['end'] != '9999-12-31 23:59:59':
            sql += ' AND `session`.`time` <= "' + filters['time']['end'] + '"'

        sql += ') ORDER BY `exhibition`.`uid` DESC'

        print(sql)

        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    print(cursor.mogrify(sql))
                    num_of_query = int(cursor.execute(sql))
                    if(num_of_query >= 1):
                        exhibitionList = cursor.fetchall()
                        print("檢視展場列表成功")
                        return True, exhibitionList

                    else:
                        print("條件不符")
                        return False, None

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False, None
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()
        return False, None

    #根據名稱查詢展場id
    def searchExhibitionIdByTitle(self, data):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT    `uid`   FROM `exhibition`   WHERE   instr(title, %(value)s) > 0 "
                    args = {"value": data}

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query > 0):
                        exhibitionIdList = cursor.fetchall()
                        print("搜尋展場成功")
                        return exhibitionIdList

                    else:
                        print("搜尋展場失敗")
                        return False

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    #根據分類查詢展場id
    def searchExhibitionIdByCategoryId(self, data):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT    `uid`   FROM `exhibition`   WHERE  `categoryId` = %(value)s "
                    args = {"value": data}

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query > 0):
                        exhibitionIdList = cursor.fetchall()
                        print("搜尋展場成功")
                        return exhibitionIdList

                    else:
                        print("搜尋展場失敗")
                        return False

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    #根據地點名稱查詢展場id
    def searchExhibitionIdByLocation(self, data):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT    `locationId`   FROM `location`   WHERE   instr(location, %(locationvalue)s) > 0 "
                    args = {"locationvalue": data}

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query > 0):
                        locationIdList = cursor.fetchall() #得到相關位置的 locationId list
                        #將每個 locationId list 用去尋找出 uid list 再組成 uid total list
                        uidTotalList = []
                        for each in locationIdList:
                            sql = "SELECT   DISTINCT   `uid`   FROM `sessionlocation`   WHERE   `locationId` =  %(locationId)s "
                            args = {"locationId": each['locationId']}

                            num_of_query = int(cursor.execute(sql, args))
                            if(num_of_query > 0):
                                exhibitionIdList = cursor.fetchall() #這是其中一個locationID 的展場list
                                for each2 in exhibitionIdList:
                                    uid = each2['uid']
                                    uidTotalList.append(uid)

                        print("搜尋展場成功，列出相關地區展場id")
                        return uidTotalList

                    else:
                        print("搜尋展場失敗，無相關地區")
                        return False

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    #根據日期查詢展場id
    def searchExhibitionIdByTime(self, time1, time2):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT   DISTINCT  `UID`   FROM `session`   WHERE  `time` BETWEEN %(value1)s AND %(value2)s ORDER BY  `time` ASC"
                    args = {"value1": time1,
                            "value2": time2
                    }

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query > 0):
                        exhibitionIdList = cursor.fetchall()
                        print("日期內相關展場如下")
                        return exhibitionIdList

                    else:
                        print("搜尋展場失敗，期間內無相關展場")
                        return False

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    def countSingleFavoriteTimes(self, uid):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT COUNT(memberId) as times FROM `favorite` WHERE `uid` = %(value)s;"
                    args = {"value": uid}

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query >= 1):
                        result = cursor.fetchone()
                        print("檢視展場收藏次數成功")

                        return True, result['times']

                    else:
                        print("檢視展場收藏次數失敗")
                        return False, None

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False, None
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()
        return False, None

    #根據展場id查詢收藏次數
    def countFavoriteTimes(self):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT `UID`, count(*) as times FROM `favorite` GROUP BY `UID`"
                    args = {}

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query >= 1):
                        result = cursor.fetchall()
                        print("檢視展場收藏次數成功")

                        return True, result

                    else:
                        print("檢視展場收藏次數失敗")
                        return False, None

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False, None
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()
        return False, None

    # 4. location.py 模組
    # ==================================================================================
    def getLocationID(self, data):
        is_get = False
        result = -1

        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    while(not is_get):
                        sql = "SELECT   `locationId`    FROM    `location`  WHERE   `locationName` = %(name)s"
                        args = {
                            "name": data.getName()
                        }

                        num_of_rows = cursor.execute(sql, args)

                        if(num_of_rows == 0):
                            sql = "INSERT INTO  `location`(`location`, `locationName`, `latitude`, `longitude`)  VALUES(%(address)s, %(name)s, %(latitude)s, %(longitude)s)"
                            args = data.getData()
                            cursor.execute(sql, args)
                            self.commit()
                        else:
                            is_get = True
                            data = cursor.fetchone()
                            result = data["locationId"]

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

        return result

    #查詢展場場次地點資訊
    def getLocationData(self, locationId):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT   *  FROM  `location`   WHERE   `locationId` = %(value)s"
                    args = {"value": locationId,}

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query == 1):
                        locationData = cursor.fetchone()
                        return locationData

                    else:
                        return False

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    # 5. member.py 模組
    # ==================================================================================
    # 註冊會員
    def addMember(self, member):
        if( self.conn() ):
            try:
                sql = "INSERT INTO  `member`    VALUES(%(account)s, %(name)s, %(password)s, %(identity)s, %(createDateTime)s)"

                # 加密密碼
                encoded_password = member.getPassword().encode("utf-8")
                print(encoded_password)
                hash_password = bcrypt.hashpw(encoded_password, bcrypt.gensalt())
                print(hash_password)


                args = {
                    'account': member.getAccount(),
                    'name': member.getName(),
                    'password' : hash_password,
                    'identity' : member.getIdentity(),
                    'createDateTime' : member.getCreateDateTime()
                }

                self.cursor().execute(sql, args)
                self.commit()

                print("新增會員成功")
                return True
            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                # print("會員已存在")
                return False
        else:
            print("Fails to connect to MySQL Server!!")
        self.close()

    # 刪除會員
    def deleteMember(self, account):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "DELETE FROM  `member`   WHERE `account` =  %(account)s"
                    args = {"account": account}


                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query == 1):
                        self.commit()
                        return True

                    else:
                        return False
            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                self.close()
                return False

        else:
            print("Fails to connect to MySQL Server!!")
            return False
        self.close()

    # 檢查會員重複
    def isMemberDuplicate(self, account):
        num_of_query = -1

        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT   *   FROM  `member`   WHERE   `account` = %(account)s"
                    args = {"account": account}

                    num_of_query = int(cursor.execute(sql, args))

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

        return True if(num_of_query == 1) else False

    # 更新會員資料
    def updateMember(self, member):
        # member object
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "UPDATE `member` SET `name` = %(name)s, `password` = %(password)s where `account` =  %(account)s"
                    args = {
                        "name": member.getName(),
                        "password":  member.getPassword(),
                        "account": member.getAccount()
                    }

                    cursor.execute(sql, args)
                    self.commit()
                    self.close()

                    return True

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                self.close()
                return False

        else:
            print("Fails to connect to MySQL Server!!")
            return False

    # 會員/管理員驗證
    def authorizeMember(self, account, password):
        num_of_query = -1

        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT   `password`, `identity`   FROM  `member`   WHERE   `account` = %(account)s;"
                    args = {
                        "account": account
                    }

                    num_of_query = int(cursor.execute(sql, args))
                    result = cursor.fetchone()

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

        if (num_of_query != 1):
            return False, "找不到此帳號", "AccountNotFoundError"
        password = password.encode("utf-8")
        hash_password = result["password"].encode("utf-8")

        if (bcrypt.checkpw(password, hash_password)):
            return True, None, None
        else:
            return False, "密碼錯誤", "WrongPasswordError"

    # 檢視會員資料
    def getMember(self, account):
        # TODO JWT 判斷是否登入
        # 拿到自己的 account
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT * FROM `member` WHERE `account` = %(account)s;"
                    args = {
                        "account": account
                    }

                    num_of_query = int(cursor.execute(sql, args))
                    if num_of_query != 1:
                        return False
                    memberData = cursor.fetchone()
                    m = Member(memberData)
                    self.close()

                    return m

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                self.close()
                return False

        else:
            print("Fails to connect to MySQL Server!!")
            return False

    # 檢視所有會員資料
    def getAllMember(self):
        # 拿到自己的 account
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT `account`, `name` FROM `member` WHERE `identity` != 'admin';"
                    cursor.execute(sql)
                    memberList = cursor.fetchall()
                    self.close()

                    return memberList

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                self.close()
                return False

        else:
            print("Fails to connect to MySQL Server!!")
            return False

    # 檢視是否為管理員
    def getMemberIdentity(self, account):
        # TODO JWT 判斷是否登入
        # 拿到自己的 account
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT `identity` FROM `member` WHERE `account` = %(account)s;"
                    args = {
                        "account": account
                    }

                    cursor.execute(sql, args)
                    memberData = cursor.fetchone()

                    self.close()

                    return memberData

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                self.close()
                return False

        else:
            print("Fails to connect to MySQL Server!!")
            return False
    # 6. organizer.py 模組
    # ==================================================================================
    def addOrganizer(self, organizer):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "INSERT INTO  `organizer` VALUES(%(unitID)s, %(uid)s, %(role)s)"
                    args = {
                        "unitID": organizer.getUnit().getID(),
                        "uid":  organizer.getUID(),
                        "role": organizer.getType()
                    }

                    cursor.execute(sql, args)
                    self.commit()

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    #查詢展場主辦單位
    def getOrganizer(self, uid):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT   `unitId`, `roleType`   FROM `organizer`   WHERE   `UID` = %(uid)s"
                    args = {"uid": uid}

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query > 0):
                        OrganizerData = cursor.fetchall()
                        return OrganizerData

                    else:
                        return False

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()


    # 7. session.py 模組
    # ==================================================================================
    def addSession(self, session):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "INSERT INTO  `session` VALUES(%(uid)s, %(seq)s, %(time)s, %(onSales)s, %(price)s, %(endTime)s)"
                    args = session.getData()

                    cursor.execute(sql, args)
                    self.commit()

            except self.mysql_error() as e:
                print('>>>>> @addSession()【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    def deleteSessionList(self, exhibitionID):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "DELETE   FROM    `session`  WHERE   `UID` = %(ID)s"
                    args = {
                        "ID": exhibitionID
                    }

                    cursor.execute(sql, args)
                    self.commit()

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    # 查詢展場場次
    def getSessionList(self, data):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT   *   FROM `session`   WHERE   `uid` = %(value)s"
                    args = {"value": data}

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query > 0):
                        sessionData = cursor.fetchall()
                        sessions = []
                        for data in sessionData:
                            s = Session()
                            s.setData(data['UID'], data['seq'], data['time'], data['onSales'], data['price'], data['endTime'])
                            sessions.append(s)
                        return sessions

                    else:
                        return False

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    # 8. sessionlocation.py 模組
    # ==================================================================================
    def addSessionLocation(self, sessionlocation):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "INSERT INTO  `sessionlocation` VALUES(%(uid)s, %(seq)s, %(locationID)s)"
                    args = {
                        "uid": sessionlocation.getSession().getUID(),
                        "seq": sessionlocation.getSession().getSeq(),
                        "locationID": sessionlocation.getLocation().getID()
                    }

                    cursor.execute(sql, args)
                    self.commit()

            except self.mysql_error() as e:
                print('>>>>> @addSession()【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    def deleteSessionLocationList(self, exhibitionID):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "DELETE   FROM    `sessionlocation`  WHERE   `UID` = %(ID)s"
                    args = {
                        "ID": exhibitionID
                    }

                    cursor.execute(sql, args)
                    self.commit()

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    #查詢展場場次地點ID
    def getSessionLocation(self, uid, seq):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT   `locationId`   FROM `sessionlocation`   WHERE   `uid` = %(uid)s && `seq` = %(seq)s"
                    args = {"uid": uid,
                            "seq": seq
                    }

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query == 1):
                        locationId = cursor.fetchone()
                        return locationId

                    else:
                        return False

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    # 9. unit.py 模組
    # ==================================================================================
    def deleteSingleExhibitionUnit(self, exhibitionID):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "DELETE   FROM    `organizer`  WHERE   `UID` = %(ID)s"
                    args = {
                        "ID": exhibitionID
                    }

                    cursor.execute(sql, args)
                    self.commit()

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    def getUnitId(self, data):
        is_get = False
        result = -1

        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    while(not is_get):
                        sql = "SELECT   `unitId`    FROM    `unit`  WHERE   `unitName` = %(name)s"
                        args = {
                            "name": data.getName()
                        }

                        num_of_rows = cursor.execute(sql, args)

                        if(num_of_rows == 0):
                            sql = "INSERT INTO  `unit`(`unitName`)  VALUES(%(name)s)"
                            args = {
                                "name": data.getName()
                            }
                            cursor.execute(sql, args)
                            self.commit()
                        else:
                            is_get = True
                            data = cursor.fetchone()
                            result = data["unitId"]

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

        return result

    #查詢展場主辦單位名稱
    def getUnitName(self, data):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT   `unitName`   FROM `unit`   WHERE   `unitId` = %(unitId)s"
                    args = {"unitId": data}

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query == 1):
                        unitName = cursor.fetchone()
                        return unitName

                    else:
                        return False

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()


    # 10. favorite.py 模組
    # ==================================================================================
    # 檢查收藏重複
    def isFavoriteDuplicate(self, exhibitionId, memberId):
        num_of_query = -1

        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT   *   FROM  `favorite`   WHERE   `UID` = %(UID)s && `memberId` = %(memberId)s"
                    args = {
                        "UID": exhibitionId,
                        "memberId": memberId
                    }

                    num_of_query = int(cursor.execute(sql, args))

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))

        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

        return True if(num_of_query == 1) else False

    #新增收藏
    def addFavorite(self, exhibitionId, memberId):
        if( self.conn() ):
            try:
                sql = "INSERT INTO  `favorite` (UID , memberId)    VALUES(%(UID)s, %(memberId)s)"
                args = {
                    'UID': exhibitionId,
                    'memberId': memberId
                }

                self.cursor().execute(sql, args)
                self.commit()

                print("新增收藏成功")
                return True
            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")
        self.close()

    #檢視會員所有收藏
    def getFavorite(self, memberId):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT   `UID`   FROM `favorite`   WHERE   `memberId` = %(value)s"
                    args = {"value": memberId}

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query > 0):
                        favoriteList = cursor.fetchall()
                        print("檢視收藏成功")

                    else:
                        favoriteList = []
                        print("無任何收藏")

                    return favoriteList

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()

    #刪除收藏
    def deleteFavorite(self, exhibitionId, memberId):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "DELETE   FROM    `favorite`  WHERE   `UID` = %(UID)s && `memberId` = %(memberId)s"
                    args = {
                        "UID":exhibitionId,
                        "memberId":memberId
                    }
                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query == 1):
                        self.commit()
                        print("刪除收藏成功")
                        return True

                    else:
                        print("刪除收藏失敗")
                        return False

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")
        self.close()

    #刪除收藏
    def deleteMemberAllFavorite(self, memberId):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "DELETE   FROM    `favorite`  WHERE   `memberId` = %(memberId)s"
                    args = {
                        "memberId":memberId
                    }
                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query > 0):
                        self.commit()
                        print("刪除該會員的收藏成功")
                        return True

                    else:
                        print("該會員無任何收藏")
                        return True

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")
        self.close()

    # 取得dashboard資訊
    def getDashboard(self):
        if( self.conn() ):
            try:
                with self.cursor() as cursor:
                    sql = "SELECT table_name, table_rows FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = %(db)s"
                    args = {"db": self.database_info["db"]}

                    num_of_query = int(cursor.execute(sql, args))
                    if(num_of_query > 0):
                        data = cursor.fetchall()
                        print("檢視收藏成功")

                    else:
                        data = []
                        print("無任何收藏")

                    return data

            except self.mysql_error() as e:
                print('>>>>> 【{}】  {!r}'.format(e.args[0], e.args[1]))
                return False
        else:
            print("Fails to connect to MySQL Server!!")

        self.close()
