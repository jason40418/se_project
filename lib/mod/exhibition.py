class Exhibition:
    def __init__(self):
        self.__uid = ""
        self.__title = ""
        self.__category = 0
        self.__discountInfo = ""
        self.__descriptionFilterHtml = ""
        self.__imageUrl = ""
        self.__webSales = ""
    
    def setData(self, data):
        self.__uid = data["UID"]
        self.__title = data["title"]
        self.__category = data["category"]
        self.__discountInfo = data["discountInfo"]
        self.__descriptionFilterHtml = data["descriptionFilterHtml"]
        self.__imageUrl = data["imageUrl"]
        self.__webSales = data["webSales"]
    
    def getUid(self):
        return self.__uid
    
    def getData(self):
        data = {
            "UID": self.__uid,
            "title": self.__title,
            "category": self.__category,
            "discountInfo": self.__discountInfo,
            "descriptionFilterHtml": self.__descriptionFilterHtml,
            "imageUrl": self.__imageUrl,
            "webSales":self.__webSales
        }

        return data