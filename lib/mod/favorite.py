from .member import Member
from .exhibition import Exhibition

class Favorite:
    # attributes
    # constructor
    # 小孟
    def __init__(self, data={}):
        self.__uid = data['UID']
        self.__memberId = data['memberId']
        self.__remind = data['remind']
        self.__remindDate = data['remindDate']

    def getFavorite(self):
        favoriteData = {
            'UID': self.__uid,
            'memberId': self.__memberId,
            'remind': self.__remind,
            'remindDate': self.__remindDate
        }

        return favoriteData

    def editFavorite(self, remind, remindDate):
        self.remind = remind
        self.remindDate = remindDate
        favoriteData = self.getFavorite()

        return favoriteData


    def getUID(self):
        return self.__uid

    def getMemberId(self):
        return self.__memberId

    def getRemind(self):
        return self.__remind

    def getRemindDate(self):
        return self.__remindDate
