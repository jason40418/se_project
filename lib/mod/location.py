class Location:
    def __init__(self):
        self.__id = 0
        self.__address = ""
        self.__name = ""
        self.__latitude = 0.0
        self.__longitude = 0.0

    def setData(self, id, address, name, latitude, longitude):
        self.__id = id
        self.__address = address
        self.__name = name

        if(latitude == None or latitude == ""):
            self.__latitude = 91.0
        else:
            self.__latitude = float(latitude)
        
        if(longitude == None or longitude == ""):
            self.__longitude = 181.0
        else:
            self.__longitude = float(longitude)
    
    def setID(self, id):
        self.__id = id
    
    def getID(self):
        return self.__id

    def getName(self):
        return self.__name
    
    def getData(self):
        data = {
            "address": self.__address,
            "name": self.__name,
            "latitude": self.__latitude,
            "longitude": self.__longitude
        }

        return data
