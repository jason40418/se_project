import bcrypt
class Member:
    # attributes
    # constructor
    # 小孟
    def __init__(self, data={}):
        self.__account = data['account']
        self.__name = data['name']
        self.__password = data['password']
        self.__identity = data['identity']
        self.__createDateTime = data['createDateTime']

    def getMember(self):
        memberData = {
            'account': self.__account,
            'name': self.__name,
            'identity': self.__identity,
            'createDateTime': self.__createDateTime
        }

        return memberData

    def editMember(self, name, oldPassword, newPassword):
        if oldPassword != None and oldPassword != "":
            oldPassword = oldPassword.encode("utf-8")
            selfPassword = self.__password.encode("utf-8")
            if not (bcrypt.checkpw(oldPassword, selfPassword)):
                print("密碼不符")
                return "passwordError"
            newPassword = newPassword.encode("utf-8") 
            newPassword = bcrypt.hashpw(newPassword, bcrypt.gensalt())
            self.__password = newPassword
        if name != None and name != "":
            self.__name = name
        memberData = self.getMember()

        return memberData

    def getAccount(self):
        return self.__account

    def getName(self):
        return self.__name

    def getPassword(self):
        return self.__password

    def getIdentity(self):
        return self.__identity
    
    def getCreateDateTime(self):
        return self.__createDateTime
