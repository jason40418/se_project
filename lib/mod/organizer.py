from .unit import Unit

class Organizer:
    def __init__(self):
        self.__uid = ""
        self.__unit = Unit()
        self.__type = ""
    
    def setUID(self, data):
        self.__uid = data
    
    def getUID(self):
        return self.__uid

    def setUnit(self, data):
        self.__unit = data
    
    def getUnit(self):
        return self.__unit
    
    def setType(self, data):
        self.__type = data
    
    def getType(self):
        return self.__type