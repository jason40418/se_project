class Session:
    def __init__(self):
        self.__uid = ""
        self.__seq = 0
        self.__time = ""
        self.__onSales = ""
        self.__price = ""
        self.__endTime = ""

    def setData(self, uid, seq, time, onSales, price, endTime):
        self.__uid = uid
        self.__seq = seq
        if(time == ""):
            self.__time = "0000-00-00 00:00:00"
        else:
            self.__time = time
        self.__onSales = onSales
        self.__price = price
        if(endTime == ""):
            self.__endTime = "9999-12-31 23:59:59"
        else:
            self.__endTime = endTime
    
    def getData(self):
        data = {
            "uid": self.__uid,
            "seq": self.__seq,
            "time": self.__time,
            "onSales": self.__onSales,
            "price": self.__price,
            "endTime": self.__endTime
        }

        return data
    
    def getUID(self):
        return self.__uid
    
    def getSeq(self):
        return self.__seq
