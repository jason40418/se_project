from .session import Session
from .location import Location

class SessionLocation:
    def __init__(self):
        self.__session = Session()
        self.__location = Location()

    def setSession(self, data):
        self.__session = data
    
    def getSession(self):
        return self.__session
    
    def setLocation(self, data):
        self.__location = data
    
    def getLocation(self):
        return self.__location
