class Unit:
    def __init__(self):
        self.__id = -1
        self.__name = ""
        self.__type = ""
    
    def setID(self, data):
        self.__id = int(data)
    
    def getID(self):
        return int(self.__id)
    
    def setName(self, data):
        self.__name = str(data)
    
    def getName(self):
        return str(self.__name)