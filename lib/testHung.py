from mod.dbmgr import DBMgr
from mod.member import Member

memberdata = {
    'account' : 'admin',
    'password' : 'admin',
    'name' : 'admin',
    'identity' : '1'
}

dbObj = DBMgr()

print(dbObj.isMemberDuplicate(memberdata['account']))

memberObj = Member(memberdata)

print(dbObj.addMember(memberObj))
