from mod.dbmgr import DBMgr
from mod.member import Member
from mod.exhibition import Exhibition
from mod.favorite import Favorite

#=================會員測試===================
memberdata = {
    'account' : 'abc123456789',
    'password' : 'pwd123456789',
    'name' : 'meng3',
    'identity' : '0'
}

dbObj = DBMgr()

print(dbObj.isMemberDuplicate(memberdata['account']))

memberObj = Member(memberdata)

print(dbObj.addMember(memberObj))

#==================展場測試====================

exhibitiondata = {
    "UID": '55a65dcd5dd8da1adc8fa99e',
    "title": '新月手繪課：梁實秋故居寫生漫遊',
    "category": 15,
    "discountInfo": '',
    "descriptionFilterHtml": '',
    "imageUrl": '',
    "webSales": ''
}
exhibitionObj = Exhibition()
exhibitionObj.setData(exhibitiondata)

exhibitionDataGettingFromDB = dbObj.getExhibition(exhibitionObj.getUid())
#展產資訊物件，與各屬性顯示
print(exhibitionDataGettingFromDB)
print("展場id：", exhibitionDataGettingFromDB["uid"])
print("展場標題：", exhibitionDataGettingFromDB["title"])
print("展場類別id：", exhibitionDataGettingFromDB["categoryId"])
#類別名稱
exhibitionCategoryName = dbObj.getCategoryName(exhibitionDataGettingFromDB["categoryId"])
print("展場類別名稱：", exhibitionCategoryName['categoryName'])
print("展場折價資訊：", exhibitionDataGettingFromDB["discountInfo"])
print("展場介紹資訊：", exhibitionDataGettingFromDB["descriptionFilterHTML"])
print("展場圖片網址：", exhibitionDataGettingFromDB["imgUrl"])
print("展場網站網址：", exhibitionDataGettingFromDB["websales"])
#主辦單位
OrginazerData = dbObj.getOrganizer(exhibitionDataGettingFromDB["uid"])
for each in OrginazerData:
    num = 1
    unitId = each['unitId']
    print("主辦單位id", num , "：", unitId)
    #主辦單位名稱
    UnitName = dbObj.getUnitName(each['unitId'])
    print("主辦單位", num , "名稱：", UnitName['unitName'])
    #單位類型
    print("主辦單位", num , "類型：", each['roleType'])

    num = num + 1
#場次
exhibitionSession =  dbObj.getSession(exhibitionObj.getUid())
for each in exhibitionSession:
    print("場次", each['seq'])
    #場次位置id
    locationId = dbObj.getSessionLocation(exhibitionObj.getUid(),each['seq'])['locationId']
    print("場次位置Id：", locationId)
    #位置資訊
    locationData = dbObj.getLocationData(locationId)
    print("場次位置地址：", locationData['location'])
    print("場次位置名稱：", locationData['locationName'])
    print("場次位置緯度：", locationData['latitude'])
    print("場次位置經度：", locationData['longitude'])
    #場次資訊
    print("開始時間：", each['time'])
    print("特價：", each['onSales'])
    print("價錢：", each['price'])
    print("結束時間：", each['endTime'])

#搜尋展場(名稱)
inputName = "圖書館"
exhibitionIdList = dbObj.searchExhibitionIdByTitle(inputName)
num = 1
for each in exhibitionIdList:
    print("展覽", num,"：", each['uid'])
    num = num + 1

#搜尋展場(分類)
inputCategotyId = 1
exhibitionIdList = dbObj.searchExhibitionIdByCategoryId(inputCategotyId)
num = 1
for each in exhibitionIdList:
    print("展覽", num,"：", each['uid'])
    num = num + 1

#搜尋展場(地區)
inputLocationName = "桃園市"
exhibitionIdList = dbObj.searchExhibitionIdByLocation(inputLocationName)
num = 1
for each in exhibitionIdList:
    print("展覽", num,"：", each)
    num = num + 1

#搜尋展場(日期)
time1 = "2018-02-15 08:30:00"
time2 = "2018-08-25 12:40:00"
exhibitionIdList = dbObj.searchExhibitionIdByTime(time1, time2)
num = 1
for each in exhibitionIdList:
    print("展覽", num,"：", each['UID'])
    num = num + 1

#==================收藏測試====================
#新增收藏
print(dbObj.isFavoriteDuplicate(exhibitionObj,memberObj.getAccount()))
print(dbObj.addFavorite(exhibitionObj, memberObj.getAccount(), 1, "2018-12-15 00:00:00"))
#檢視收藏
favoriteList = dbObj.getFavorite(memberObj.getAccount())
num = 1
for each in favoriteList:
    print("收藏", num,"：", each['UID'])
    num = num + 1
#刪除收藏
#dbObj.deleteFavorite(exhibitionObj,memberObj.getAccount())