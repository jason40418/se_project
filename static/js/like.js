$(() => {
  $('.favorite').on('click',function() {
    let data = {"UID": this.id}

    // 取消收藏
    if ($(this).hasClass('like1')) {
      $.ajax({
        url: '/api/favorites',
        method: 'DELETE',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: (reuslt) => {
          console.log(reuslt.msg);
          alert(reuslt.msg);

          $(this).removeClass('like1');
          $(this).addClass('like');
        },
        error: (error) => {
          let errorData = error.responseJSON;
          alert(errorData.error_type + ": " + errorData.error_msg);
          if(errorData.error_type === 'LoginRequiredError')
            window.location.href = '/login';
        }
      });
    // 新增回來
    } else {
      $.ajax({
        url: '/api/favorites',
        method: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: (reuslt) => {
          console.log(reuslt.msg);
          alert(reuslt.msg);

          $(this).removeClass('like');
          $(this).addClass('like1');
        },
        error: (error) => {
          let errorData = error.responseJSON;
          alert(errorData.error_type + ": " + errorData.error_msg);
          if(errorData.error_type === 'LoginRequiredError')
            window.location.href = '/login';
        }
      });
    }
  });
});
