$(() => {
  const accountInput = $('#accountInput');
  const passwordInput = $('#passwordInput');

  $('#loginButton').on('click', login);

  function login() {
    let account = accountInput.val();
    let password = passwordInput.val();
    if (account.trim() == "" || password.trim() == "") {
      alert('請輸入帳號或密碼！');
      return;
    }
    let data = {account, password};
    console.log(data);
    $.ajax({
      url: '/api/login',
      method: 'post',
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify(data),
      success: (reuslt) => {
        console.log(reuslt.msg);
        alert(reuslt.msg);
        window.location.href = "/index";
      },
      error: (error) => {
        let errorData = error.responseJSON;
        alert(errorData.error_type + ": " + errorData.error_msg);
      }
    });
  }
});
