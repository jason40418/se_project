$(() => {
  const nameInput = $('#nameInput');
  const oldPasswordInput = $('#oldPasswordInput');
  const newPasswordLi = $('#newPasswordLi');
  const newPasswordInput = $('#newPasswordInput');

  $('#saveButton').on('click', register);
  $('#oldPasswordInput').on('keyup', handlePassword);

  function handlePassword() {
    let oldPassword = oldPasswordInput.val();
    if (oldPassword.length != 0) {
      newPasswordLi.show();
    } else {
      newPasswordLi.hide();
    }
  }

  function register() {
    let name = nameInput.val();
    let oldPassword = oldPasswordInput.val();
    let newPassword = newPasswordInput.val();

    // check type
    let errorList = [];

    if (name.trim() == "") {
      errorList.push("記得取個「名字」喔喔喔");
    } else {
      if (name.trim() != name) {
        errorList.push("「名字」前後不能有空格喔喔喔");
      }
      if (name.length > 20) {
        errorList.push("「名字」過長喔喔喔（最多 20 個字）");
      }
    }

    if (oldPassword.trim() != "") {
      if (newPassword.trim() == "") {
        errorList.push("記得輸入「新的密碼」喔喔喔");
      } else if (newPassword.indexOf(' ') >= 0) {
        errorList.push("「新的密碼」不能有空格喔喔喔");
      }
    }


    if (errorList.length != 0) {
      console.log(errorList);
      let alertMsg = "";
      for (let error of errorList) {
        alertMsg += error + "\n";
      }
      alert(alertMsg);
      return;
    }

    let data = {name, oldPassword, newPassword};
    console.log(data);
    $.ajax({
      url: '/api/member',
      method: 'patch',
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify(data),
      success: (reuslt) => {
        console.log(reuslt.msg);
        alert(reuslt.msg);
        window.location.href = "/member";
      },
      error: (error) => {
        console.log(error);
        let errorData = error.responseJSON;
        alert(errorData.error_type + ": " + errorData.error_msg);
        if (error.status == 403) {
          window.location.href = "/login";
        }

      }
    });
  }
});
