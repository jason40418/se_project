$(() => {
  const nameInput = $('#nameInput');
  const emailInput = $('#emailInput');
  const passwordInput = $('#passwordInput');
  const passwordCheckInput = $('#passwordCheckInput');

  $('#registerButton').on('click', register);

  function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  function register() {
    let name = nameInput.val();
    let email = emailInput.val();
    let password = passwordInput.val();
    let passwordCheck = passwordCheckInput.val();

    // check type
    let errorList = [];

    if (name.trim() == "") {
      errorList.push("記得取個「名字」喔喔喔");
    } else {
      if (name.trim() != name) {
        errorList.push("「名字」前後不能有空格喔喔喔");
      }
      if (name.length > 20) {
        errorList.push("「名字」過長喔喔喔（最多 20 個字）");
      }
    }

    if (email.trim() == "") {
      errorList.push("記得輸入「信箱」喔喔喔");
    } else if (email.trim() != email) {
      errorList.push("「信箱」前後不能有空格喔喔喔");
    } else if (!validateEmail(email)) {
      errorList.push("「信箱」不符合格式喔喔喔");
    }

    if (password.trim() == "") {
      errorList.push("記得輸入「密碼」喔喔喔");
    } else if (password.indexOf(' ') >= 0) {
      errorList.push("「密碼」不能有空格喔喔喔");
    }
    if (password !== passwordCheck) {
      errorList.push("「密碼」不相符喔喔喔");
    }

    if (errorList.length != 0) {
      console.log(errorList);
      let alertMsg = "";
      for (let error of errorList) {
        alertMsg += error + "\n";
      }
      alert(alertMsg);
      return;
    }

    let data = {name, password, "account": email};
    console.log(data);
    $.ajax({
      url: '/api/register',
      method: 'post',
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify(data),
      success: (reuslt) => {
        console.log(reuslt.msg);
        alert(reuslt.msg);
        window.location.href = "/login";
      },
      error: (error) => {
        let errorData = error.responseJSON;
        alert(errorData.error_type + ": " + errorData.error_msg);
      }
    });
  }
});
